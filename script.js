"use strict";

/**
 * Variables
 */

const body = document.querySelector(".body-js");
const themeSpan = document.querySelectorAll(".header__numbers--js span");
const display = document.querySelector(".display");
const numberButtons = document.querySelectorAll(".calculator__btn--js-num");
const dotButton = document.querySelector(".calculator__btn--js-dot");
const oprButtons = document.querySelectorAll(".calculator__btn--js-opr");
const delButton = document.querySelector(".calculator__btn--js-del");
const resetButton = document.querySelector(".calculator__btn--js-reset");
const equButton = document.querySelector(".calculator__btn--js-equ");

themeSpan.forEach((elem) => {
  elem.addEventListener("click", () => themeSpanClick(body, elem));
});

numberButtons.forEach((elem) => {
  elem.addEventListener("click", () => inputNumber(display, elem));
});

dotButton.addEventListener("click", () => inputDot(display));

oprButtons.forEach((elem) => {
  elem.addEventListener("click", () => inputOperator(display, elem));
});

delButton.addEventListener("click", () => undo(display));

resetButton.addEventListener("click", () => reset(display));

equButton.addEventListener("click", () => result(display));

/**
 * Functions
 */

/**
 * Function that removes all classes from body element than add theme class
 * with number from span element in dataset theme.
 * @param {HTMLBodyElement} body
 * @param {HTMLSpanElement} elem
 */
function themeSpanClick(body, elem) {
  // Remove all possible themes from body element
  body.classList.remove("theme-1", "theme-2", "theme-3");

  const { theme } = elem.dataset;

  // Add theme to body element
  body.classList.add(`theme-${theme}`);
}

/**
 * Input number into the display
 * @param {HTMLDivElement} display
 * @param {HTMLButtonElement} elem
 */
function inputNumber(display, elem) {
  const { key: number } = elem.dataset;

  if (display.textContent == "0") display.textContent = number;
  else display.textContent += number;
}

/**
 * Input dot into the display
 * @param {HTMLDivElement} display
 */
function inputDot(display) {
  const inputs = display.textContent.split(" ");
  const lastNum = inputs[inputs.length - 1];

  if (!Number.isNaN(+lastNum) && !lastNum.match(/,/))
    display.textContent += ",";
}

/**
 * Function the inputs operator into the display
 * @param {HTMLDivElement} display
 * @param {HTMLButtonElement} operator
 */
function inputOperator(display, operator) {
  // If display is 0 and operator is - input minus operator
  if (display.textContent == "0") {
    if (operator.dataset.key == "-") display.textContent = " -";
  }
  // If operator is already enter operator you can only enter unary minus
  // operator
  else if (display.textContent.endsWith(" ")) {
    if (operator.dataset.key == "-") display.textContent += " -";
  }
  // If display ends with numbers from 0-9 enter operator
  else if (display.textContent.match(/[0-9]$/)) {
    display.textContent += ` ${operator.dataset.key} `;
  }
}

/**
 * Function that reset calculator
 * @param {HTMLDivElement} display
 */
function reset(display) {
  display.textContent = "0";
}

/**
 * Function that undoes previous input
 * @param {HTMLDivElement} display
 */
function undo(display) {
  // If display ends with number delete only one last character
  if (
    display.textContent.match(/[0-9]$/) ||
    display.textContent.endsWith(",") ||
    display.textContent == "-"
  ) {
    display.textContent = display.textContent.slice(
      0,
      display.textContent.length - 1
    );
  }

  // If display ends " " delete last 3 characters because its going to be
  // something like " - "
  else if (display.textContent.endsWith(" ")) {
    display.textContent = display.textContent.slice(
      0,
      display.textContent.length - 3
    );
  }
  // If display ends with " -" delete last 2 characters
  else if (display.textContent.endsWith(" -")) {
    display.textContent = display.textContent.slice(
      0,
      display.textContent.length - 2
    );
  }

  if (display.textContent == "") display.textContent = "0";
}

/**
 *
 * @param {HTMLDivElement} display
 */
function result(display) {
  let { textContent } = display;

  textContent = textContent.replace(/,/g, ".");

  try {
    let result = math.round(math.evaluate(textContent), 5);

    result = String(result).replace(/\./g, ",");

    display.textContent = result;
  } catch {
    display.textContent = 0;
    console.error("Error occurred in calculation");
    return;
  }
}
